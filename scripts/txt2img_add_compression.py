from omegaconf import OmegaConf
from ldm.util import instantiate_from_config

import argparse
import pathlib

import torch
import torch.nn as nn

def load_model_from_config(config, ckpt, verbose=False):
    print(f"Loading model from {ckpt}")
    pl_sd = torch.load(ckpt, map_location="cpu")
    sd = pl_sd["state_dict"]
    model = instantiate_from_config(config.model)
    m, u = model.load_state_dict(sd, strict=False)
    if len(m) > 0 and verbose:
        print("missing keys:")
        print(m)
    if len(u) > 0 and verbose:
        print("unexpected keys:")
        print(u)


    return model


def main():

    ## Load the base model
    config = OmegaConf.load("configs/latent-diffusion/txt2img-1p4B-eval.yaml")
    model = load_model_from_config(config, "models/ldm/text2img-large/model.ckpt")

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

    text_encoder = model.cond_stage_model


    ## Add a new token
    new_token = opt.comp_token
    
    text_encoder.tknz_fn.tokenizer.add_tokens([new_token])
    n_vocab_new = len(text_encoder.tknz_fn.tokenizer)
    

    ## Define the loss and optimizer
    #comp_loss = nn.MSELoss()
    comp_loss = nn.L1Loss()

    ## Expand the embedding layer of the text encoder

    # Create a new embedding layer
    token_emb_new = nn.Embedding(n_vocab_new, 1280).to(device)
    nn.init.normal_(token_emb_new.weight, std=0.02)
    
    # Get the old one
    token_emb_old = text_encoder.transformer.token_emb

    # Keep the old weights
    token_emb_new.weight.data[:n_vocab_new -1] = token_emb_old.weight.data

    text_encoder.transformer.token_emb = token_emb_new

    text_encoder = text_encoder.to(device)
    
    ## Training ##
    
    print('---------------------------- Start Training -------------------------------')
    print()


    ## Get the ground truth. This will be the encoded
    ## sentence 'Tom Hanks with sunglasses wearing a red cowboy hat'
    z_target = text_encoder(opt.prompt).detach().to(device)
    
    text_encoder.train()

    # Freeze the paramters
    for param in text_encoder.parameters():
        param.requires_grad = False

    # Only train the embedding layers
    text_encoder.transformer.token_emb.weight.requires_grad = True


    optim = torch.optim.NAdam(text_encoder.parameters(),
                             lr = opt.lr,
                             betas = (0.9, 0.999)
                            )

    #lr_scheduler = torch.optim.lr_scheduler.StepLR(optim, step_size = 100, gamma = 0.7)
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optim, step_size = opt.lr_steps, gamma = opt.gamma)
    #lr_scheduler = torch.optim.lr_scheduler.StepLR(optim, step_size = 300, gamma = 0.5)    


    for epoch in range(opt.n_epochs):

        ## encode the compression token
        z = text_encoder(opt.comp_token)

        ## Compute loss
        #loss = comp_loss(z, z_target)
        loss = comp_loss(z, z_target)
        
        optim.zero_grad()

        loss.backward()

        # Zero the gradients of other embeddings
        text_encoder.transformer.token_emb.weight.grad[:n_vocab_new-1].zero_()

        # Amplify the gradients of the trained token
        #comp_grad = text_encoder.transformer.token_emb.weight.grad[-1]        
        #text_encoder.transformer.token_emb.weight.grad[-1] = 128 * comp_grad
        
        optim.step()
        lr_scheduler.step()
        
        print(f'({epoch}) LOSS is', loss.item())
        
    
    print('---------------------------- Training Ended -------------------------------')
    print()

    ## Now save the training embedding

    # Get the trained token embedding from the embedding matrix

    trained_emb = torch.Tensor(text_encoder.transformer.token_emb.weight[-1])
    
    # Save it
    outdir = pathlib.Path(opt.outdir)
    outdir.mkdir(exist_ok = True)
    save_path = outdir / (new_token + '.pt')
    
    torch.save(trained_emb, save_path)

    print(f'Compressed token saved to {save_path}')

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--prompt",
        type=str,
        help="the prompt to compress"
    )

    parser.add_argument(
        "--comp-token",
        type=str,
        help="the token representing the compression"
    )

    parser.add_argument(
        "--n-epochs",
        type=int,
        default=1000,
        help="The training epochs"
    )

    parser.add_argument(
        "--lr",
        type=float,
        default=2,
        help="The learning rate"
    )

    parser.add_argument(
        "--lr-steps",
        type=int,
        default=100,
        help="the step size of the LR Step scheduler"
    )

    parser.add_argument(
        "--gamma",
        type=float,
        default=1,
        help="the multiplier of the LR Step scheduler"
    )
    
    parser.add_argument(
        "--outdir",
        type=str,
        help="dir to save compressions to",
    )
    opt = parser.parse_args()


    main()
    
