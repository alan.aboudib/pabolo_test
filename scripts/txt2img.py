import argparse, os, sys, glob
import torch
import numpy as np
from omegaconf import OmegaConf
from PIL import Image
from tqdm import tqdm, trange
from einops import rearrange
from torchvision.utils import make_grid

from ldm.util import instantiate_from_config
from ldm.models.diffusion.ddim import DDIMSampler
from ldm.models.diffusion.plms import PLMSSampler

import torch.nn as nn

def load_model_from_config(config, ckpt, verbose=False):
    print(f"Loading model from {ckpt}")
    pl_sd = torch.load(ckpt, map_location="cpu")
    sd = pl_sd["state_dict"]
    model = instantiate_from_config(config.model)
    m, u = model.load_state_dict(sd, strict=False)
    if len(m) > 0 and verbose:
        print("missing keys:")
        print(m)
    if len(u) > 0 and verbose:
        print("unexpected keys:")
        print(u)


    model.eval()
    return model


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--prompt",
        type=str,
        nargs="?",
        default="a painting of a virus monster playing guitar",
        help="the prompt to render"
    )

    parser.add_argument(
        "--outdir",
        type=str,
        nargs="?",
        help="dir to write results to",
        default="outputs/txt2img-samples"
    )
    parser.add_argument(
        "--ddim_steps",
        type=int,
        default=200,
        help="number of ddim sampling steps",
    )

    parser.add_argument(
        "--plms",
        action='store_true',
        help="use plms sampling",
    )

    parser.add_argument(
        "--ddim_eta",
        type=float,
        default=0.0,
        help="ddim eta (eta=0.0 corresponds to deterministic sampling",
    )
    parser.add_argument(
        "--n_iter",
        type=int,
        default=1,
        help="sample this often",
    )

    parser.add_argument(
        "--H",
        type=int,
        default=256,
        help="image height, in pixel space",
    )

    parser.add_argument(
        "--W",
        type=int,
        default=256,
        help="image width, in pixel space",
    )

    parser.add_argument(
        "--n_samples",
        type=int,
        default=4,
        help="how many samples to produce for the given prompt",
    )

    parser.add_argument(
        "--scale",
        type=float,
        default=5.0,
        help="unconditional guidance scale: eps = eps(x, empty) + scale * (eps(x, cond) - eps(x, empty))",
    )
    opt = parser.parse_args()


    config = OmegaConf.load("configs/latent-diffusion/txt2img-1p4B-eval.yaml")  # TODO: Optionally download from same location as ckpt and chnage this logic
    model = load_model_from_config(config, "models/ldm/text2img-large/model.ckpt")  # TODO: check path

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    print(model.cond_stage_model.tknz_fn)
    model = model.to(device)

    ## Add a new token
    print(f'vocab size is {len(model.cond_stage_model.tknz_fn.tokenizer)}')    
    model.cond_stage_model.tknz_fn.tokenizer.add_tokens(['<hanks>'])
    n_vocab_new = len(model.cond_stage_model.tknz_fn.tokenizer)
    print(f'vocab size is {n_vocab_new}')

    
    sentence = 'Tom Hanks with sunglasses wearing a red cowboy hat'

    ## Get the ground truth. This will be the encoded
    ## sentence 'Tom Hanks with sunglasses wearing a red cowboy hat'
    z_target = model.cond_stage_model(sentence).detach().to(device)
    print(z_target)
    ## Define the MSE loss and optimizer
    hanks_loss = nn.MSELoss()
    #hanks_loss = nn.L1Loss()

    lr = 1#e-1
    n_epochs = 2000
    
    
    ## Expand the embedding layer of the text encoder

    # Get the tokens ID of the sentence excluding [sep], [cls] and [pad]
    token_ids = model.cond_stage_model.tknz_fn(sentence)
    token_ids = torch.tensor([t.item() for t in token_ids[0] if t not in [101, 102, 0]])

    
    # Create a new embedding layer
    token_emb_new = nn.Embedding(n_vocab_new, 1280)

    # Get the old one
    token_emb_old = model.cond_stage_model.transformer.token_emb

    # Get the mean embedding of the sentence tokens
    sentence_emb = token_emb_old.weight[token_ids]
    sentence_emb = torch.mean(sentence_emb, dim = 0)
    
    print(f'sentence emb {sentence_emb}')
    
    # Keep the old weights
    token_emb_new.weight.data[:n_vocab_new -1] = token_emb_old.weight.data

    # the new token embedding is initialized with the mean embedding
    # of the original sentence
    print('before', token_emb_new.weight.data[-1])    
    #token_emb_new.weight.data[-1] = sentence_emb
    print('after', token_emb_new.weight.data[-1])

    model.cond_stage_model.transformer.token_emb = token_emb_new.to(device)

    ## encode <hanks>
    z_hanks = model.cond_stage_model('<hanks>')
    print('hank', z_hanks)
    print('LOSS is', hanks_loss(z_target, z_hanks))

    ## Training
    print('----------- Start Training -------------------------------')
    print()

    model.cond_stage_model.train()

    # Freeze the paramters
    for param in model.cond_stage_model.parameters():
        param.requires_grad = False

    # Only train the embedding layers
    model.cond_stage_model.transformer.token_emb.weight.requires_grad = True

    #optim = torch.optim.Adam(model.cond_stage_model.parameters(),
    #                         lr = lr,
    #                         betas = (0.9, 0.999)
    #                        )

    optim = torch.optim.NAdam(model.cond_stage_model.parameters(),
                             lr = lr,
                             betas = (0.9, 0.999)
                            )
    
    #optim = torch.optim.SGD(model.cond_stage_model.parameters(),
    #                        lr = lr,
    #                        momentum = 0.9
    #                        )

    lr_scheduler = torch.optim.lr_scheduler.StepLR(optim, step_size = 100, gamma = 0.9)
    #lr_scheduler = torch.optim.lr_scheduler.CyclicLR(optim, base_lr=0.001, max_lr=2, step_size_up=10, mode='triangular2', cycle_momentum = False)

    # Train loop
    for epoch in range(n_epochs):

        
        ## encode <hanks>
        z_hanks = model.cond_stage_model('<hanks>')

        ## Compute loss
        #loss = hanks_loss(z_hanks[:,0,:], z_target[:,0,:])
        loss = hanks_loss(z_hanks, z_target)        
        
        optim.zero_grad()

        loss.backward()
        model.cond_stage_model.transformer.token_emb.weight.grad[:n_vocab_new-1].zero_()
        optim.step()
        lr_scheduler.step(epoch)
        
        print(f'({epoch})LOSS is', loss.item())

        # Keep the old weights
        #model.cond_stage_model.transformer.token_emb.weight.data[:n_vocab_new -1] = token_emb_old.weight.data
        #model.cond_stage_model.transformer.token_emb.weight.requires_grad = True        

    model.cond_stage_model.eval()

    ## End of training
    model = model.to(device)

    if opt.plms:
        sampler = PLMSSampler(model)
    else:
        sampler = DDIMSampler(model)

    os.makedirs(opt.outdir, exist_ok=True)
    outpath = opt.outdir

    prompt = opt.prompt


    sample_path = os.path.join(outpath, "samples")
    os.makedirs(sample_path, exist_ok=True)
    base_count = len(os.listdir(sample_path))

    all_samples=list()
    with torch.no_grad():
        with model.ema_scope():
            uc = None
            if opt.scale != 1.0:
                uc = model.get_learned_conditioning(opt.n_samples * [""])
            for n in trange(opt.n_iter, desc="Sampling"):
                c = model.get_learned_conditioning(opt.n_samples * [prompt])
                #c[:,1:,:] = 0                
                print('*****', c, c.size(), c.requires_grad)
                print('tttttt', z_target)

                shape = [4, opt.H//8, opt.W//8]
                samples_ddim, _ = sampler.sample(S=opt.ddim_steps,
                                                 conditioning=c,
                                                 batch_size=opt.n_samples,
                                                 shape=shape,
                                                 verbose=False,
                                                 unconditional_guidance_scale=opt.scale,
                                                 unconditional_conditioning=uc,
                                                 eta=opt.ddim_eta)

                x_samples_ddim = model.decode_first_stage(samples_ddim)
                x_samples_ddim = torch.clamp((x_samples_ddim+1.0)/2.0, min=0.0, max=1.0)

                for x_sample in x_samples_ddim:
                    x_sample = 255. * rearrange(x_sample.cpu().numpy(), 'c h w -> h w c')
                    Image.fromarray(x_sample.astype(np.uint8)).save(os.path.join(sample_path, f"{base_count:04}.png"))
                    base_count += 1
                all_samples.append(x_samples_ddim)


    # additionally, save as grid
    grid = torch.stack(all_samples, 0)
    grid = rearrange(grid, 'n b c h w -> (n b) c h w')
    grid = make_grid(grid, nrow=opt.n_samples)

    # to image
    grid = 255. * rearrange(grid, 'c h w -> h w c').cpu().numpy()
    Image.fromarray(grid.astype(np.uint8)).save(os.path.join(outpath, f'{prompt.replace(" ", "-")}.png'))

    print(f"Your samples are ready and waiting four you here: \n{outpath} \nEnjoy.")
