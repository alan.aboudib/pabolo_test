from setuptools import setup, find_packages

setup(
    name='latent-diffusion',
    version='0.0.1',
    description='',
    packages=find_packages(),
    install_requires=[
        'torch',
        'numpy',
        'tqdm',
        'omegaconf',
        'einops',
        'torchvision',
        'pytorch_lightning==1.4.0',
        'torchmetrics==0.6.0',
        'taming-transformers @ git+https://github.com/CompVis/taming-transformers.git@master#egg=taming-transformers',
        'clip @ git+https://github.com/openai/CLIP.git@main#egg=clip',
        'kornia',
        'transformers',

    ],
)
